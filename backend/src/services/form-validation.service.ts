import { Validator } from "node-input-validator"

// función que permite validar cómodamente los formularios
export async function formValidation(body: any, validation: any) {
    const validate = new Validator(body, validation)
    return {
        success: await validate.check(),
        errors: validate.errors
    }
} 