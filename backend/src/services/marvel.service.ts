import { MARVEL_PRIVATE_KEY, MARVEL_PUBLIC_KEY, MARVEL_URL } from "../config"
import moment from "moment"
import md5 from "md5"
import axios from "axios"

export class MarvelService {
    private public_key = MARVEL_PUBLIC_KEY
    private private_key = MARVEL_PRIVATE_KEY
    private url = MARVEL_URL

    private async getQuery (path: string, aditionalParams?: any) {
        const url = this.url + path
        const ts = moment().unix()
        const hash = md5(ts + this.private_key + this.public_key)
        const params = {
            ts,
            hash,
            apikey: this.public_key
        }
        // agrego los filtros a la petición
        for (const key in aditionalParams) {
            if (Object.prototype.hasOwnProperty.call(aditionalParams, key)) {
                params[key] = aditionalParams[key];
            }
        }
        const res = await axios.get(url, {
            params
        })
        return res.data
    }

    // metodos de consulta hacia la api de marvel
    getCharacters = (nameStartsWith: string): any => this.getQuery('characters', { nameStartsWith })
    getCharacter = (characterId: string): any => this.getQuery(`characters/${ characterId }`)
}