import moment from "moment"
import { ObjectId } from "mongoose"
import { JWT_DURATION, JWT_KEY } from "../config"
import { decode, encode } from "jwt-simple"
import { IResponseJWT } from "../interfaces/response-jwt.interface"

export class JwtService {
    private key: string = JWT_KEY
    private duration: number = +JWT_DURATION

    // metodo para crear el token de autenticación
    createToken (sub: string | ObjectId, email: string): IResponseJWT {
        const payload = {
            sub,
            email,
            iat: moment().unix(),
            exp: moment().add(this.duration, 'seconds').unix(),
        }
        return {
            token: encode(payload, this.key),
            payload
        }
    }

    decodeToken (token: string) {
        let payload: any = {}
        try {
            payload = decode(token, this.key, true)
            if (payload.exp < moment().unix()) return { success: false, expired: true, data: { status: 403, message: 'Expired token' }, payload }
            return { success: true, data: payload }
        } catch (e: any) {
            return { success: false, data: { status: 401, message: 'Invalid Token', e: e.message } }
        }
    }
}