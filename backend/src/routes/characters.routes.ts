import { Router } from "express";
import { CharacterController } from "../controllers/character.controller";
import { JWTAuth } from "../middlewares/jwt-auth.middleware";

const characterController = new CharacterController()

export const CharacterRouter = Router()
/**
 * @swagger
 * components:
 *  schemas:
 *      StoreCharacterDTO:
 *          type: object
 *          properties:
 *              marvelId:
 *                  type: string
 *                  example: 1011334
 *  parameters:
 *      name_start_with:
 *          in: path
 *          name: name_start_with
 *          required: true
 *          schema:
 *              type: string
 *          description: Filtro para obtener los personajes de marvel
 *      character_marvel_id:
 *          in: path
 *          name: id
 *          schema:
 *              type: string
 *          description: identificación de marvel para el personaje
 *      characterId:
 *          in: path
 *          name: id
 *          schema:
 *              type: string
 *          description: identificación de base de datos para el personaje
 */

/**
 * @swagger
 * tags:
 *  name: Personajes
 */

/**
 * @swagger
 * /character/filter/{name_start_with}:
 *  get:
 *      security:
 *          - bearerAuth: []  
 *      summary: Obtener los personajes de marvel
 *      tags: [Personajes]
 *      parameters:
 *          - $ref: '#/components/parameters/name_start_with'
 *      responses:
 *          200: 
 *              description: Array de personajes de marvel
 */
CharacterRouter.get('/filter/:name_start_with', JWTAuth, characterController.index)

/**
 * @swagger
 * /character/stored:
 *  get:
 *      security:
 *          - bearerAuth: []  
 *      summary: Obtener los personajes de marvel almacenados en base de datos
 *      tags: [Personajes]
 *      responses:
 *          200: 
 *              description: Array de personajes de marvel alacenados en base de datos
 */
CharacterRouter.get('/stored', JWTAuth, characterController.getCharacterStoreds)

/**
 * @swagger
 * /character/by-id/{id}:
 *  get:
 *      security:
 *          - bearerAuth: []  
 *      summary: Obtener los personajes de marvel
 *      tags: [Personajes]
 *      parameters:
 *          - $ref: '#/components/parameters/character_marvel_id'
 *      responses:
 *          200: 
 *              description: Array de personajes de marvel
 */
CharacterRouter.get('/by-id/:id', JWTAuth, characterController.show)

/**
 * @swagger
 * /character:
 *  post:
 *      security:
 *          - bearerAuth: []  
 *      summary: Guardar un personaje de marvel en la base de datos
 *      tags: [Personajes]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/StoreCharacterDTO'
 *      responses:
 *          200: 
 *              description: Objeto del personaje almacenado
 */
CharacterRouter.post('/', JWTAuth, characterController.store)

/**
 * @swagger
 * /character/{id}:
 *  delete:
 *      security:
 *          - bearerAuth: []  
 *      summary: Borrar un personaje de la base de datos
 *      tags: [Personajes]
 *      parameters:
 *          - $ref: '#/components/parameters/characterId'
 *      responses:
 *          200: 
 *              description: Información del documento borrado 
 */
CharacterRouter.delete('/:id', JWTAuth, characterController.destroy)