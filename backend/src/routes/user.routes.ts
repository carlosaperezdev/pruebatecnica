import { Router } from "express"
import { UserController } from "../controllers/user.controller"
import { JWTAuth } from "../middlewares/jwt-auth.middleware"

const userController = new UserController()
export const UserRouter = Router()

/**
 * @swagger
 * components:
 *      schemas:
 *          ProfileDTO:
 *              type: object
 *              properties:
 *                  email:
 *                      type: string
 *                      format: email
 *                      example: user@example.com
 */

/**
 * @swagger
 * tags:
 *  name: Usuario
 */

/**
 * @swagger
 * /user/profile:
 *  get:
 *      security:
 *          - bearerAuth: []  
 *      summary: Obtener mis datos de perfil
 *      tags: [Usuario]
 *      responses:
 *          200: 
 *              description: Objeto con la información del usuairo
 */
UserRouter.get('/profile', JWTAuth, userController.myProfile)

/**
 * @swagger
 * /user/profile:
 *  put:
 *      security:
 *          - bearerAuth: []  
 *      summary: Editar datos de perfil
 *      tags: [Usuario]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/ProfileDTO'
 *      responses:
 *          200: 
 *              description: Objeto con la información del usuairo
 */
UserRouter.put('/profile', JWTAuth, userController.editMyProfile)