import { Router } from "express";
import { AuthController } from "../controllers/auth.controller";

export const AuthRouter = Router()

const authController = new AuthController()

/**
 * @swagger
 * components:
 *      schemas:
 *          loginDTO:
 *              type: object
 *              properties:
 *                  email:
 *                      type: string
 *                      format: email
 *                      example: user@example.com
 *                  password:
 *                      type: string
 *                      minLength: 8
 */

/**
 * @swagger
 * tags:
 *  name: Autenticación
 */

/**
 * @swagger
 * /auth/login:
 *  post:
 *      summary: Login en la aplicación
 *      tags: [Autenticación]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/loginDTO'
 *      responses:
 *          200: 
 *              description: Objeto con el token JWT y el payload del token
 */
AuthRouter.post('/login', authController.login)

/**
 * @swagger
 * /auth/signup:
 *  post:
 *      summary: Registro en la aplicación
 *      tags: [Autenticación]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/loginDTO'
 *      responses:
 *          200: 
 *              description: Usuario ya creado
 */
AuthRouter.post('/signup', authController.signup)