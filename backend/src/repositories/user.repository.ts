import { Response } from "express";
import { ObjectId } from "mongoose";
import { AuthRequest } from "../interfaces/request.interface";
import { IUser } from "../interfaces/user.interface";
import { User } from "../models/user.model";
import { formValidation } from "../services/form-validation.service";

// método para obtener el usuario por email
export const getUser = async (filter: any): Promise<IUser | null> => {
    return await User.findOne(filter)
}

// método para almacenar en base de datos al usuario
export const storeUser = async (email: string, password: string): Promise<IUser>=> {
    const user = new User({ email, password })
    return await user.save()
}

// método para actualizar la información del usuario
export const updateUser = async (userId: string | ObjectId, newData: any) => {
    return await User.findByIdAndUpdate(userId, newData, { new: true })
}

// método para obtener el perfil de la persona autenticada
export const getMyProfile = async (req: AuthRequest, res: Response) => {
    const user = req.user.sub
    const myUser = await getUser({ _id: user })
    if (!myUser) return res.status(404).send({ message: 'Usuario no encontrado' })
    myUser.password = undefined
    return  res.status(200).send(myUser)
}

// método para editar poder editar mi perfil
export const editMyProfile = async (req: AuthRequest, res: Response) => {
    try {
        const user = req.user.sub
        const body = req.body
        const validForm = await formValidation(body, {
            email: 'required|email'
        })
        if (!validForm.success) return res.status(400).send({ message: 'Formulario inválido', ...validForm })
        // validar que el email nuevo no esté tomado
        const userExists = await getUser({ email: body.email, _id: { $ne: user } })
        if (userExists) return res.status(400).send({ message: 'Email ya está tomado' })
        const userUpdated = await updateUser(user, { email: body.email })
        userUpdated.password = undefined
        return res.status(200).send(userUpdated)
    } catch (err) {
        return res.status(500).send({ message: 'Hubo un error en el servidor', err: err.message })
    }
}