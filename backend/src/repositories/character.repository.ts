import { Response } from "express";
import { AuthRequest } from "../interfaces/request.interface";
import { Character } from "../models/character.model";
import { MarvelService } from "../services/marvel.service";

// obtener los personajes de marvel con el filtro de nombre
export const getCharacters = async (req: AuthRequest, res: Response) => {
    try {
        const nameStartsWith = req.params.name_start_with
        const marvelService = new MarvelService()
        const marvelResponse = await marvelService.getCharacters(nameStartsWith)
        return res.status(200).send(marvelResponse)
    } catch (err) {
        return res.status(500).send({ message: 'Hubo un error en el servidor', err: err.message })
    }
}

// obtener un personaje de marvel por su Id
export const getCharacter = async (req: AuthRequest, res: Response) => {
    try {
        const characterId = req.params.id
        const marvelService = new MarvelService()
        const marvelResponse = await marvelService.getCharacter(characterId)
        return res.status(200).send(marvelResponse)
    } catch (err) {
        return res.status(500).send({ message: 'Hubo un error en el servidor', err: err.message })
    }
}

// obtener los personajes guardados en base de datos
export const getCharactersStoreds = async (req: AuthRequest, res: Response) => {
    try {
        const user = req.user.sub
        // obtener los documentos
        return res.status(200).send(await Character.find({ user }))
    } catch (err: any) {
        return res.status(500).send({ message: 'Hubo un error en el servidor', err: err.message })
    }
}

// almacenar personaje de marvel en base de datos
export const storeCharacter = async (req: AuthRequest, res: Response) => {
    try {
        const user = req.user.sub
        // obtener el id proporcionado de marvel y validar
        const marvelId = req.body.marvelId
        if (!marvelId) return res.status(400).send({ message: 'Formulario inválido' })
        // buscar si el personaje ya está guardado por el usuario
        const characterExists = await Character.findOne({ user, marvelId })
        if (characterExists) return res.status(400).send({ message: 'Ya tienes agregado al personaje' })
        // buscar por el id marvel el personaje
        const marvelService = new MarvelService()
        const marvelCharacterResponse = await marvelService.getCharacter(marvelId)
        // si no se encuentra el personaje para directamente al catch
        // si existe en la api sigue el flujo con normalidad guardando la información relevante del personaje
        const character = new Character({
            user,
            ...marvelCharacterResponse.data.results[0],
            marvelId: marvelCharacterResponse.data.results[0].id
        })
        return res.status(200).send(await character.save())
    } catch (err) {
        return res.status(500).send({ message: 'Hubo un error en el servidor', err: err.message })
    }
}

// Poder borrar un personaje de la base de datos
export const deleteCharacter = async (req: AuthRequest, res: Response) => {
    try {
        const user = req.user.sub
        const _id = req.params.id
        // Buscar y eliminar de la base de datos
        const dataDelete = await Character.findOneAndDelete({ _id, user })
        return res.status(200).send(dataDelete)
    } catch (err) {
        return res.status(500).send({ message: 'Hubo un error en el servidor', err: err.message })
    }
}