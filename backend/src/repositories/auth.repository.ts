import { compareSync } from "bcrypt"
import { Request, Response } from "express"
import { IResponseJWT } from "../interfaces/response-jwt.interface"
import { formValidation } from "../services/form-validation.service"
import { JwtService } from "../services/jwt.service"
import { getUser, storeUser, updateUser } from "./user.repository"

export const login = async (req: Request, res: Response) => {
    const body = req.body
    const validForm = await formValidation(body, {
        email: 'required|email',
        password: 'required|minLength:8'
    })
    if (!validForm.success) return res.status(400).send({ message: 'Formulario inválido', ...validForm })
    // Obtener el usuario con el correo
    const user = await getUser({ email: body.email })
    if (!user) return res.status(404).send({ message: 'Usuario o contraseña incorrecta' })
    // validar contraseña
    if(!compareSync(body.password, user.password || '')) return res.status(404).send({ message: 'Usuario o contraseña incorrecta' })
    // crear token de autenticación
    const jwtService = new JwtService()
    const dataToken: IResponseJWT = jwtService.createToken(user._id, user.email)
    // actualizar ultimo logeo
    await updateUser(user._id, { lastLogin: Date.now() })
    return res.status(200).send(dataToken)
}

export const signup = async (req: Request, res: Response) => {
    try {
        const body = req.body
        const validForm = await formValidation(body, {
            email: 'required|email',
            password: 'required|minLength:8'
        })
        if (!validForm.success) return res.status(400).send({ message: 'Formulario inválido', ...validForm })
        // Obtener el usuario con el correo
        const userExists = await getUser({ email: body.email })
        if (userExists) return res.status(404).send({ message: 'Usuario ya existe' })
        // crear usuario
        const userStored = await storeUser(body.email, body.password)
        // borrar contraseña de la respuesta
        userStored.password = undefined
        return res.status(200).send(userStored)
    } catch (err: any) {
        return res.status(500).send({ message: 'Hubo un error en el servidor', err: err.message })
    }
}
