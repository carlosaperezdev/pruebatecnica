import { NextFunction, Response } from "express"
import { JwtService } from "../services/jwt.service"

export function JWTAuth(req: any, res: Response, next: NextFunction) {
    let token = req.headers.authorization
    // validar extructura del token
    if (!token || token.indexOf('Bearer ') < 0) return res.status(403).send({ message: 'No token' })
    token = token.replace('Bearer ', '')
    const jwtService = new JwtService()
    // decodificar token del usuario
    const response = jwtService.decodeToken(token)
    if (!response.success) return res.status(response.data.status).send(response.data)
    // asignarlo al request para poder ser utilizado en las rutas 
    req.user = response.data
    next()
}