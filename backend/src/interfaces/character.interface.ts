import { Document } from "mongoose";

export interface ICharacter extends Document {
    user: string
    marvelId: number
    name: string
    description: string
    modified: Date
    thumbnail: any
}