import { Request } from "express";
import { IJWTPayload } from "./jwt-payload.interface";

export interface AuthRequest extends Request {
    user: IJWTPayload
}