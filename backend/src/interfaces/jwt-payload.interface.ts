export interface IJWTPayload {
    sub: string
    email: string
    iat: number
    exp: number
}