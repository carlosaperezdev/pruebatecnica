import { hashSync } from "bcrypt";
import { model, Schema } from "mongoose";
import { IUser } from "../interfaces/user.interface";

const UserSchema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
    lastLogin: Date
})

// middleware al almacenar el usuario que encripta la contraseña 
UserSchema.pre('save', function (next) {
    const user = this
    user.password  = hashSync(user.password, 12)
    next()
})

export const User = model<IUser>('User', UserSchema)