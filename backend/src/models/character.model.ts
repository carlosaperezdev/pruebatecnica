import { model, Schema } from "mongoose";
import { ICharacter } from "../interfaces/character.interface";

const CharacterSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    marvelId: { type: Number, required: true },
    name: { type: String, required: true},
    description: String,
    modified: Date,
    thumbnail: Schema.Types.Mixed
})

export const Character = model<ICharacter>('Character', CharacterSchema)