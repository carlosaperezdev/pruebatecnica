import { app } from "./app"
import { APP_PORT, MONGO_URL } from "./config"
import mongoose, { ConnectOptions } from 'mongoose'

if (!MONGO_URL) {
    console.log('MongoDB url does not exists')
    process.exit(1)
}
// creacion del servidor
mongoose.Promise = global.Promise
mongoose.connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true} as ConnectOptions)
.then(() => app.listen(APP_PORT, () => console.log(`Server run in port: ${ APP_PORT }`)))