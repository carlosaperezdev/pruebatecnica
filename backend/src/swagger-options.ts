import { Options } from "swagger-jsdoc";

export const SwaggerOptions: Options = {
    swaggerDefinition: {
        openapi: '3.0.1',
        info: {
            title: 'Backend Prueba técnica',
            version: '1.0.0',
            description: 'Backend proporcionado para la prueba técnica'
        },
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: 'http',
                    scheme: 'bearer',
                    bearerFormat: 'JWT',
                }
            }
        }
    },
    apis: ["./src/routes/*.ts"]
}