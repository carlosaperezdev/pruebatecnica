import { Response } from "express";
import { AuthRequest } from "../interfaces/request.interface";
import { deleteCharacter, getCharacter, getCharacters, getCharactersStoreds, storeCharacter } from "../repositories/character.repository";

export class CharacterController {
    index = (req: AuthRequest, res: Response) => getCharacters(req, res)
    getCharacterStoreds = (req: AuthRequest, res: Response) => getCharactersStoreds(req, res)
    show = (req: AuthRequest, res: Response) => getCharacter(req, res)
    store = (req: AuthRequest, res: Response) => storeCharacter(req, res)
    destroy = (req: AuthRequest, res: Response) => deleteCharacter(req, res)
}