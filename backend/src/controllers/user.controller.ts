import { Response } from "express";
import { AuthRequest } from "../interfaces/request.interface";
import { editMyProfile, getMyProfile } from "../repositories/user.repository";

export class UserController {
    myProfile = (req: AuthRequest, res: Response) => getMyProfile(req, res)
    editMyProfile = (req: AuthRequest, res: Response) => editMyProfile(req, res)
}