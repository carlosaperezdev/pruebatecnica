import { Request, Response } from "express";
import { login, signup } from "../repositories/auth.repository";

export class AuthController {
    login = async (req: Request, res: Response) => await login(req, res)

    signup = async (req: Request, res: Response) => await signup(req, res)
}
