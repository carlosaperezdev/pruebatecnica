import { config } from "dotenv"
config()

export const NODE_ENV = process.env.NODE_ENV || 'development'
export const APP_PORT = process.env.APP_PORT || '8080'
export const MONGO_URL = process.env.MONGO_URL || null
export const JWT_KEY = process.env.JWT_KEY || "59h8mS*FUjOx27LUaN!ykZRlH!ZcbGv86i@fX%*zxfq!6d@sdj"
export const JWT_DURATION = process.env.JWT_DURATION || 2629800
export const MARVEL_PUBLIC_KEY = process.env.MARVEL_PUBLIC_KEY || null
export const MARVEL_PRIVATE_KEY = process.env.MARVEL_PRIVATE_KEY || null
export const MARVEL_URL = process.env.MARVEL_URL || 'https://gateway.marvel.com/v1/public/'