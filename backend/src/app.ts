import express from 'express'
import morgan from 'morgan'
import cors from 'cors'

// swagger
import swaggerUI from 'swagger-ui-express'
import { SwaggerOptions } from './swagger-options'
import swaggerJSDoc from 'swagger-jsdoc'

// cargar rutas
import { AuthRouter } from './routes/auth.routes'
import { UserRouter } from './routes/user.routes'
import { CharacterRouter } from './routes/characters.routes'

// configuración
export const app = express()

app.use(cors())
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
const  specs = swaggerJSDoc(SwaggerOptions)

// rutas
app.use('/auth', AuthRouter)
app.use('/character', CharacterRouter)
app.use('/user', UserRouter)
app.use('/docs', swaggerUI.serve, swaggerUI.setup(specs))
