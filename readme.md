# Aplicación de prueba técnica de marvel

## Backend

Backend proporcionado para la prueba técnica.
para acceder al backend se debe ingresar el siguente comando:

```
cd backend/
```

### Modo de desarrollo
1. Descargar los paquetes necesarios.
   
    ```
    npm install
    ```

2. Se debe ejecutar el comando para levantar el servidor de desarrollo.
   
    ```
    npm run dev
    ```

3. Empezar a desarrollar sobre él

### Modo de producción
Para ejecutar el modo producción se debe tener Docker instalado, y seguir los pasos

1. Crear la imagen de docker
   
    ```
    docker build -t [NOMBRE] .
    ```

2. Ejecutar imagen de docker
   
    ```
    docker run -d -p 3000:3000 [NOMBRE]
    ```

#### Swagger
Para acceder a la documentación de la api se debe ingresar a la siguente ruta:

```
http://localhost:3000/docs
```

## Frontend
Frontend proporcionado para la prueba técnica.
para acceder al backend se debe ingresar el siguente comando:

```
cd frontend/
```
    

### Modo de desarrollo
1. Descargar los paquetes necesarios.
   
    ```
    npm install
    ```

2. Se debe ejecutar el comando para levantar el cliente de angular.
   
    ```
    npm start
    ```

3. Empezar a desarrollar sobre él

### Modo de producción
Para ejecutar el modo producción se debe tener Docker instalado, y seguir los pasos

1. Crear la imagen de docker
   
    ```
    docker build -t [NOMBRE] .
    ```

2. Ejecutar imagen de docker
   
    ```
    docker run -d -p 80:80 [NOMBRE]
    ```