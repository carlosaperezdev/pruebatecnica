import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading',
  template: `
  <div class="text-center">
    <div class="spinner-border" [ngClass]="color" style="width: 4rem; height: 4rem;" role="status">
      <span class="visually-hidden">Loading...</span>
    </div>
  </div>
  `,
})
export class LoadingComponent {
  @Input() public color = 'text-primary';
}
