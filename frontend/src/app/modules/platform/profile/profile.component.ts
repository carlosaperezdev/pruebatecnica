import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BackendService } from 'src/app/services/backend.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profile: any;
  loading: boolean;
  myForm: FormGroup;

  constructor(
    private backendService: BackendService
  ) {
    this.profile = null
    this.loading = true
  }

  ngOnInit(): void {
    this.getProfile()
  }
  
  async getProfile() {
    try {
      this.loading = true
      this.profile = await this.backendService.getProfile()
      this.buildForm()
    } catch (e: any) {
      Swal.fire('Error', e.error.message, 'error')
    }
    this.loading = false
  }
  buildForm() {
    this.myForm = new FormGroup({
      email: new FormControl(this.profile.email, [
        Validators.required,
        Validators.email
      ])
    })
  }

  onSubmit () {
    Swal.fire({
      title: 'Actualizar perfil',
      text: "¿Estás seguro de actualizar email?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Actualizar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.backendService.putProfile(this.myForm.value).then(response => {
          this.getProfile()
          Swal.fire('Éxito', 'Correo actualizado con éxito', 'success')
        }).catch(e => {
          Swal.fire('Error', e.error.message, 'error')
        })
      }
    })
  }

}
