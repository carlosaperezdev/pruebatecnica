import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// módulos internos
import { ComponentsModule } from 'src/app/components/components.module';

import { PlatformRoutingModule } from './platform-routing.module';
import { PlatformComponent } from './platform.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [
    PlatformComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    PlatformRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
  ]
})
export class PlatformModule { }
