import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PlatformComponent } from './platform.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: PlatformComponent,
    children: [
      {
        path: 'characters',
        loadChildren: () => import('./characters/characters.module').then(m => m.CharactersModule)
      },
      { path: 'profile', component: ProfileComponent },
      { path: '**', pathMatch: 'full', redirectTo: 'characters' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlatformRoutingModule { }
