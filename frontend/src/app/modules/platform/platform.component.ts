import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.css']
})
export class PlatformComponent {

  constructor (
    private authService: AuthService
  ) { }

  logout () {
    Swal.fire({
      title: 'Cerrar sesión',
      text: "¿Estás seguro de cerrar sesión?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Cerrar sesión',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.authService.logout()
        Swal.fire(
          'Éxito',
          'Sesión cerrada correctamente',
          'success'
        )
      }
    })
  }
}
