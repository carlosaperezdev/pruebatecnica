import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getImage'
})
export class GetImagePipe implements PipeTransform {

  transform(value: { path: string, extension: string }, size: string = 'standard_medium'): string {
    return (value.path) ?  `${ value.path }/${ size }.${ value.extension }` : ''
    
  }

}
