import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'validateStored'
})
export class ValidateStoredPipe implements PipeTransform {

  transform(marvelId: number, storeds: any[]): boolean {
    return Boolean(storeds.find(s => s.marvelId == marvelId))
  }

}
