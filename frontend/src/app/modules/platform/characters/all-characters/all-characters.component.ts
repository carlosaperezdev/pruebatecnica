import { Component, OnInit } from '@angular/core'
import { BackendService } from 'src/app/services/backend.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-all-characters',
  templateUrl: './all-characters.component.html',
  styles: [
  ]
})
export class AllCharactersComponent implements OnInit {

  loading: boolean
  dtOptions: DataTables.Settings = {}
  // string de busqueda
  searchResult: string = ''
  // id del personaje que ses está guardando a tiempo real
  charaterSaving: number
  // array de los personajes
  charactersStoreds: any[]
  characters: any[]
  
  constructor (
    private backendService: BackendService
  ) {
    this.charaterSaving = 0
    this.loading = false
    this.characters = []
    this.charactersStoreds = []
    this.configDatatable()
  }

  ngOnInit(): void {
    this.getCharactersSaved()
  }

  configDatatable () {
    this.dtOptions = {
      searching: false,
      columnDefs: [
        { orderable: false, targets: 2 },
        { orderable: false, targets: 3 }
      ]
    }
  }

  async getCharactersSaved() {
    try {
      this.charactersStoreds = await this.backendService.getCharacterStoreds()
    } catch (e: any) {
      Swal.fire('Error', e.error.message, 'error')
    }
  }

  async getCharacters () {
    try {
      this.characters = []
      this.loading = true
      const response = await this.backendService.getCharactersBySearch(this.searchResult)
      this.characters = response.data.results
    } catch (e: any) {
      Swal.fire('Error', e.error.message, 'error')
    }
    this.loading = false
  }


  async saveCharacter (marvelId: number) {
    try {
      this.charaterSaving = marvelId
      const response = await this.backendService.saveCharacter({ marvelId })
      Swal.fire('Éxito', 'Personaje almacenado con éxito', 'success')
      this.getCharacters()
      this.getCharactersSaved()
    } catch (e: any) {
      Swal.fire('Error', e.error.message, 'error')
    }
    this.charaterSaving = 0
  }

}
