import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail-character',
  templateUrl: './detail-character.component.html',
  styles: [
  ]
})
export class DetailCharacterComponent {
  @ViewChild('content') private  content: TemplateRef<any>
  
  character: any

  constructor(private modalService: NgbModal) { }

  open(character: any) {
    this.character = character
    this.modalService.open(this.content)
  }
}
