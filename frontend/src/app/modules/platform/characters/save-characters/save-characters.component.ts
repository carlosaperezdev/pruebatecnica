import { Component, OnInit, ViewChild } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import Swal from 'sweetalert2';
import { DetailCharacterComponent } from '../detail-character/detail-character.component';

@Component({
  selector: 'app-save-characters',
  templateUrl: './save-characters.component.html',
  styles: [
  ]
})
export class SaveCharactersComponent implements OnInit {
  // componente de detalle de personaje
  @ViewChild(DetailCharacterComponent, { static: false }) detailCharacterComponent: DetailCharacterComponent
  
  loading: boolean
  dtOptions: DataTables.Settings = {}
  // array de personajes guardados
  characters: any[]
  // personaje que se está eliminando a tiempo real
  characterIdDeleting: string

  constructor (
    private backendService: BackendService
  ) {
    this.characterIdDeleting = ''
    this.loading = true
    this.configDatatable()
  }

  ngOnInit (): void {
    this.getCharactersStoreds()
  }

  configDatatable () {
    this.dtOptions = {
      columnDefs: [
        { orderable: false, targets: 2 },
        { orderable: false, targets: 3 }
      ],
      rowCallback: (row: Node, data: any, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', (e) => {
          // donde se haga click en la fila solo muestre el detalle cuando no se pulse el botón de eliminar
          if (!$(e.target).is('button')) self.showCharacterDetail(data[0])
        });
        return row;
      }
    }
  }

  async getCharactersStoreds () {
    try {
      this.loading = true
      this.characters = await this.backendService.getCharacterStoreds()
    } catch (e: any) {
      Swal.fire('Error', e.error.message, 'error')
    }
    this.loading = false
  }

  async deleteCharacter (character: any) {
    Swal.fire({
      title: 'Eliminar personaje',
      text: `¿Estás seguro de eliminar a ${ character.name }?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.characterIdDeleting = character._id
        this.backendService.deleteCharacter(character._id).then(response => {
          Swal.fire('Éxito', 'Personaje eliminado con éxito', 'success')
          this.getCharactersStoreds()
        }).catch(e => {
          Swal.fire('Error', e.error.message, 'error')
        })
      }
    })
  }
  // llamado al modal detalle
  showCharacterDetail (name: string) {
    // busco por el nombre del personaje en el array de personajes
    const character = this.characters.find(c => c.name == name)
    this.detailCharacterComponent.open(character)
  }

}
