import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// módulos internos
import { ComponentsModule } from 'src/app/components/components.module';
// módulos externos
import { DataTablesModule } from 'angular-datatables';

// componentes y rutas
import { CharactersRoutingModule } from './characters-routing.module';
import { CharactersComponent } from './characters.component';
import { AllCharactersComponent } from './all-characters/all-characters.component';
import { SaveCharactersComponent } from './save-characters/save-characters.component';
import { DetailCharacterComponent } from './detail-character/detail-character.component';

// pipes
import { GetImagePipe } from './pipes/get-image.pipe';
import { ValidateStoredPipe } from './pipes/validate-stored.pipe';

@NgModule({
  declarations: [
    CharactersComponent,
    AllCharactersComponent,
    SaveCharactersComponent,
    GetImagePipe,
    ValidateStoredPipe,
    DetailCharacterComponent
  ],
  imports: [
    CommonModule,
    CharactersRoutingModule,
    DataTablesModule,
    FormsModule,
    ComponentsModule
  ]
})
export class CharactersModule { }
