import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CharactersComponent } from './characters.component';
import { AllCharactersComponent } from './all-characters/all-characters.component';
import { SaveCharactersComponent } from './save-characters/save-characters.component';

const routes: Routes = [
  {
    path: '',
    component: CharactersComponent,
    children: [
      { path: 'search', component: AllCharactersComponent },
      { path: 'save', component: SaveCharactersComponent },
      { path: '**', pathMatch: 'full', redirectTo: 'search' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CharactersRoutingModule { }
