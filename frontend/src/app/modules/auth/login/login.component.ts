import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { BackendService } from 'src/app/services/backend.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  myForm: FormGroup
  loading: boolean
  constructor(
    private backendService: BackendService,
    private authService: AuthService,
    private router: Router
  ) {
    this.loading = false
  }

  ngOnInit (): void {
    this.buildForm()
  }

  buildForm (): void {
    this.myForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    })
  }

  async onSubmit (): Promise<void> {
    if (this.myForm.invalid) return
    this.loading = true
    try {
      const response = await this.backendService.postLogin(this.myForm.value)
      this.authService.login(response);
      this.router.navigate(['/platform']);
    } catch (e: any) {
      Swal.fire('Error', e.error.message, 'error')
    }
    this.loading = false
  }
}
