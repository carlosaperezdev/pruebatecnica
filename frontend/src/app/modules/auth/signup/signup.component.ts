import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { BackendService } from 'src/app/services/backend.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles: [
  ]
})
export class SignupComponent implements OnInit {

  myForm: FormGroup
  loading: boolean
  
  constructor (
    private backendService: BackendService,
    private authService: AuthService,
    private router: Router
  ) {
    this.loading = false
  }

  ngOnInit (): void {
    this.buildForm()
  }

  buildForm (): void {
    this.myForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    })
  }
  async onSubmit (): Promise<void> {
    if (this.myForm.invalid) return
    this.loading = true
    try {
      const response = await this.backendService.postSignup(this.myForm.value)
      this.authService.login(response);
      Swal.fire('Éxito', 'Bienvenido a sistema, por favor inicia sesión', 'success')
      this.router.navigate(['/auth']);
    } catch (e: any) {
      Swal.fire('Error', e.error.message, 'error')
    }
    this.loading = false
  }

}
