import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { AuthService } from './auth.service'

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private url: string

  constructor(
    private http: HttpClient,
    private userAuth: AuthService
  ) {
    this.url = environment.backend
  }

  private getQuery(query: string): Promise<any> {
    const url = this.url + query
    const headers = new HttpHeaders().set('authorization', `Bearer ${ this.userAuth.getToken() }`)
    return this.http.get(url, { headers }).toPromise()
  }

  private postQuery(path: string, params: any, token: boolean = true): Promise<any> {
    const url = `${this.url + path}`
    let headers: HttpHeaders
    if (token) {
      headers = new HttpHeaders().set('Content-Type', 'application/json')
      .set('authorization', `Bearer ${ this.userAuth.getToken() }`)
    } else {
      headers = new HttpHeaders().set('Content-Type', 'application/json')
    }
    return this.http.post(url, params, { headers } ).toPromise()
  }

  private putQuery(path: string, params: any): Promise<any> {
    const url = `${this.url + path}`
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
      .set('authorization', `Bearer ${ this.userAuth.getToken() }`)
    return this.http.put(url, params, { headers } ).toPromise()
  }

  private deleteQuery(query: string): Promise<any> {
    const url = this.url + query
    const headers = new HttpHeaders().set('authorization', `Bearer ${ this.userAuth.getToken() }`)
    return this.http.delete(url, { headers }).toPromise()
  }

  // rutas para la autenticación
  postLogin = (dataLogin: any) => this.postQuery('auth/login', dataLogin, false)
  postSignup = (dataSignup: any) => this.postQuery('auth/signup', dataSignup, false)

  // rutas para el perfil
  getProfile = () => this.getQuery('user/profile')
  putProfile = (dataProfile: any) => this.putQuery('user/profile', dataProfile)

  // rutas para los personajes
  getCharactersBySearch = (dataSearch: string) => this.getQuery(`character/filter/${ dataSearch }`)
  getCharacterStoreds = () => this.getQuery('character/stored')
  saveCharacter = (dataCharacter: any) => this.postQuery('character', dataCharacter)
  deleteCharacter = (characterId: string) => this.deleteQuery(`character/${ characterId }`)
}
