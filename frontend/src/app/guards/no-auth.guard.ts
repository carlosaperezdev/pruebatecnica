import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class NoAuthGuard implements CanActivate {
  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  canActivate() {
    // valido que no exista el token 
    if (this.auth.getIdentity() && this.auth.getToken()) {
      this.router.navigate(['/platform']);
      return false;
    } else {
      return true;
    }
  }
}
