import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private auth: AuthService,
    private router: Router
  ) { }
  
  canActivate() {
    // valido la existencia del token 
    if (!(this.auth.getIdentity() && this.auth.getToken())) {
      this.router.navigate(['/auth']);
      return false;
    } else {
      return true;
    }
  }
  
}
